<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Avaliação de Software Acessível</title>
        <link rel="shortcut icon" href="/acc/include/favicon.ico" />
        <link rel="icon" href="/acc/include/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="/acc/css/main.css">
        <link rel="stylesheet" href="/acc/css/bootstrap.css">
        <script src="/acc/js/jquery-2.1.4.min.js"></script>
        <script src="/acc/js/jquery-browser.js"></script>
        <script src="/acc/js/main.js"></script>
        <script src="/acc/js/acc.js"></script>
        
    </head>
    <body>
        <!-- Audio Area -->
        <audio loop id="audioArea">
            <source src="acc/sounds/beep-7.mp3" type="audio/mpeg">
            Seu navegador não possui suporte ao elemento audio
        </audio>
        <!-- Audio Centro -->
        <audio loop id="audioCentro">
            <source src="acc/sounds/beep-2.mp3" type="audio/mpeg">
            Seu navegador não possui suporte ao elemento audio
        </audio>  
        <!-- Audio Borda -->
        <audio loop id="audioBorda">
            <source src="acc/sounds/beep-4.mp3" type="audio/mpeg">
            Seu navegador não possui suporte ao elemento audio
        </audio>
        <div class="container">         
            <div class="content container-fluid" id="mainContent">

                <?php
                require("acc/view/telaInicial.php");
                ?>

            </div>
        </div>   
    </body>
</html>