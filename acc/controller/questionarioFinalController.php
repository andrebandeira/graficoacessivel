<?php

require ('../bd/conexao.php');
if (!isset($_SESSION)) {
    session_start('questionario');
}
$usuario = $_SESSION['idUsuario'];
$origem = $_SESSION['origem'];
if ($usuario != null) {
    buscarListaRespostas4($usuario);
    if ($origem == 'questionarioFinal') {
        deletarRespostasAnteriores($usuario, ['15', '16', '17', '18', '19', '20', '21', '22', '23']);
        $respostas = $_POST;
        foreach ($respostas as $pergunta => $resposta) {
            if ($pergunta[0] == 'P') {
                $numPergunta = $pergunta[1].$pergunta[2];
                if ($numPergunta == '21' ||$numPergunta == '22' || $numPergunta == '23') {
                    salvarRespostasUsuarioTexto($usuario, $numPergunta, $resposta);
                } else {
                    salvarRespostasUsuario($usuario, $numPergunta, $resposta);
                }
            }
        }
        require ('../view/telaFinal.php');
    } else if ($origem == 'avaliacao') {
        $listaRespostas = $_SESSION['listaRespostas'];
        if ($listaRespostas) {
            $tecnologiaAssistiva = Array();
            foreach ($listaRespostas as $resposta) {
                if ($resposta[0] == 15) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $entendimentoTarefas = '15PNR';
                    } else if ($resposta[1] == 'Discordo Totalmente') {
                        $entendimentoTarefas = '15DT';
                    } else if ($resposta[1] == 'Discordo') {
                        $entendimentoTarefas = '15D';
                    } else if ($resposta[1] == 'Nem Concordo Nem Discordo') {
                        $entendimentoTarefas = '15NCND';
                    } else if ($resposta[1] == 'Concordo') {
                        $entendimentoTarefas = '15C';
                    } else if ($resposta[1] == 'Concordo Totalmente') {
                        $entendimentoTarefas = '15CT';
                    }
                } else if ($resposta[0] == 16) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $facilidadeConclusao = '16PNR';
                    } else if ($resposta[1] == 'Discordo Totalmente') {
                        $facilidadeConclusao = '16DT';
                    } else if ($resposta[1] == 'Discordo') {
                        $facilidadeConclusao = '16D';
                    } else if ($resposta[1] == 'Nem Concordo Nem Discordo') {
                        $facilidadeConclusao = '16NCND';
                    } else if ($resposta[1] == 'Concordo') {
                        $facilidadeConclusao = '16C';
                    } else if ($resposta[1] == 'Concordo Totalmente') {
                        $facilidadeConclusao = '16CT';
                    }
                } else if ($resposta[0] == 17) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $dificuldadeConclusao = '17PNR';
                    } else if ($resposta[1] == 'Discordo Totalmente') {
                        $dificuldadeConclusao = '17DT';
                    } else if ($resposta[1] == 'Discordo') {
                        $dificuldadeConclusao = '17D';
                    } else if ($resposta[1] == 'Nem Concordo Nem Discordo') {
                        $dificuldadeConclusao = '17NCND';
                    } else if ($resposta[1] == 'Concordo') {
                        $dificuldadeConclusao = '17C';
                    } else if ($resposta[1] == 'Concordo Totalmente') {
                        $dificuldadeConclusao = '17CT';
                    }
                } else if ($resposta[0] == 18) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $imagemMental = '18PNR';
                    } else if ($resposta[1] == 'Discordo Totalmente') {
                        $imagemMental = '18DT';
                    } else if ($resposta[1] == 'Discordo') {
                        $imagemMental = '18D';
                    } else if ($resposta[1] == 'Nem Concordo Nem Discordo') {
                        $imagemMental = '18NCND';
                    } else if ($resposta[1] == 'Concordo') {
                        $imagemMental = '18C';
                    } else if ($resposta[1] == 'Concordo Totalmente') {
                        $imagemMental = '18CT';
                    }
                } else if ($resposta[0] == 19) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $imagemMentalGrafico = '19PNR';
                    } else if ($resposta[1] == 'Discordo Totalmente') {
                        $imagemMentalGrafico = '19DT';
                    } else if ($resposta[1] == 'Discordo') {
                        $imagemMentalGrafico = '19D';
                    } else if ($resposta[1] == 'Nem Concordo Nem Discordo') {
                        $imagemMentalGrafico = '19NCND';
                    } else if ($resposta[1] == 'Concordo') {
                        $imagemMentalGrafico = '19C';
                    } else if ($resposta[1] == 'Concordo Totalmente') {
                        $imagemMentalGrafico = '19CT';
                    }
                } else if ($resposta[0] == 20) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $representacaoInformacoes = '20PNR';
                    } else if ($resposta[1] == 'Discordo Totalmente') {
                        $representacaoInformacoes = '20DT';
                    } else if ($resposta[1] == 'Discordo') {
                        $representacaoInformacoes = '20D';
                    } else if ($resposta[1] == 'Nem Concordo Nem Discordo') {
                        $representacaoInformacoes = '20NCND';
                    } else if ($resposta[1] == 'Concordo') {
                        $representacaoInformacoes = '20C';
                    } else if ($resposta[1] == 'Concordo Totalmente') {
                        $representacaoInformacoes = '20CT';
                    }
                } else if ($resposta[0] == 21) {
                    $dificuldades = pg_unescape_bytea($resposta[2]);
                } else if ($resposta[0] == 22) {
                    $sugestoes = pg_unescape_bytea($resposta[2]);
                } else if ($resposta[0] == 23) {
                    $potencial = pg_unescape_bytea($resposta[2]);
                }
            }
        }
        require ('../view/questionarioFinal.php');
    }
} else {
    require ('../view/telaInicial.php');
}

function salvarRespostasUsuario($usuario, $pergunta, $resposta) {
    //Salva os dados do usuario
    $insert = 'INSERT INTO CDRESPOSTA (CDRESPERGUNTA,
                                        CDRESUSUARIO,
                                        CDRESRESPOSTA)
                      VALUES (' . $pergunta . ','
            . $usuario . ''
            . ',\'' . $resposta . '\');';
    pg_query(CONN, $insert);
}

function salvarRespostasUsuarioTexto($usuario, $pergunta, $resposta) {
    //Salva os dados do usuario
    $insert = 'INSERT INTO CDRESPOSTA (CDRESPERGUNTA,
                                        CDRESUSUARIO,
                                        CDRESTEXTO)
                      VALUES (' . $pergunta . ','
            . $usuario . ''
            . ',\'' . $resposta . '\');';
    pg_query(CONN, $insert);
}

function deletarRespostasAnteriores($usuario, $perguntas) {
    foreach ($perguntas as $pergunta) {
        //Salva os dados do usuario
        $delete = ' DELETE FROM CDRESPOSTA
                WHERE  CDRESUSUARIO = ' . $usuario . ' AND
                       CDRESPERGUNTA = ' . $pergunta;
        pg_query(CONN, $delete);
    }
}

function buscarListaRespostas4($usuario) {
    //Busca as perguntas
    $queryBusca = ' SELECT CDRESPERGUNTA,
                           CDRESRESPOSTA,
                           CDRESTEXTO
                    FROM   CDRESPOSTA
                    WHERE  CDRESUSUARIO = ' . $usuario . ';';

    $resultBusca = pg_query(CONN, $queryBusca);
    $respostas = array();
    while ($row = pg_fetch_row($resultBusca)) {
        $respostas[] = $row;
    }
    //Salva na sessão
    $_SESSION['listaRespostas'] = $respostas;
}
