<?php

//Pega a conexão com o banco
require ('../bd/conexao.php');

//Inicializa as variáveis do usuario
$usuario = null;
$ipUsuario = '\'UNKNOWN\'';

//Veriifica se existe usuário na sessão
if (isset($_SESSION['$ipUsuario'])) {
    $ipUsuario = $_SESSION['$ipUsuario'];
} else if (isset($_POST['meuIP'])) {
    //Verifica se foi informado um ip na requisição
    $ipUsuario = '\'' . $_POST['meuIP'] . '\'';
    $_SESSION['$ipUsuario'] = $ipUsuario;
}

//Verifica se é uma continuação de tentativa anterior
$cta = isset($_POST['TentativaAnterior']) ? $_POST['TentativaAnterior'] : '';

//Se tem ip do usuário busca no banco
if ($ipUsuario != 'UNKNOWN') {
    //Busca o ip do usuário
    $queryBusca = 'SELECT CDUSUID
                   FROM   CDUSUARIO 
                   WHERE  CDUSUIP = ' . $ipUsuario;

    $resultBusca = pg_query(CONN, $queryBusca);

    $row = pg_fetch_row($resultBusca);
} else {
    $row = false;
}

//Se encontrou o usuário no  banco e é uma continuação de tentativa anterior
if ($row && $cta == 'SIM') {
    //Salva o usuário na sessão
    $usuario = $row[0];
    $_SESSION['idUsuario'] = $usuario;
    $_SESSION['origem'] = 'login';
    //Busca a lista de perguntas e respostas
    buscarListaPerguntas();
    buscarListaRespostas1($usuario);
    //Encaminha pro questionario inicial
    require('../controller/questionarioInicialController.php');
} else if ($row && $cta == '') {
    //Se encontrou o usuário no  banco pergunta pro usuário se quer continuar 
    //a avaliação anterior ou é uma nova avaliação
    require('../view/tentivaAnterior.php');
} else {
    //Insere o usuário no BD
    $insert = 'INSERT INTO CDUSUARIO (CDUSUIP) VALUES (' . $ipUsuario . ');';
    $result = pg_query(CONN, $insert);
    $last_id = pg_fetch_array(pg_query("SELECT CURRVAL('cdusuario_cdusuid_seq')"));
    $usuario = $last_id[0];
    $_SESSION['idUsuario'] = $usuario;
    $_SESSION['origem'] = 'login';
    //Busca a lista de perguntas e respostas
    buscarListaPerguntas();
    buscarListaRespostas1($usuario);
    require('../controller/questionarioInicialController.php');
}

function buscarListaPerguntas() {
    //Busca as perguntas
    $queryBusca = ' SELECT CDPERID,
                           CDPERIMAGEM,
                           CDPEROPCAO1,
                           CDPEROPCAO2,
                           CDPEROPCAO3,
                           CDPEROPCAO4,
                           CDPERCENTROX,
                           CDPERCENTROY
                    FROM   CDPERGUNTA
                    WHERE  CDPEROPCAO1 IS NOT NULL
                    ORDER BY CDPERID;';

    $resultBusca = pg_query(CONN, $queryBusca);
    $perguntas = array();

    while ($row = pg_fetch_row($resultBusca)) {
        $perguntas[] = $row;
    }

    if ($perguntas) {
        $ultima = array_pop($perguntas);
        $penultima = array_pop($perguntas);
        $antepenultima = array_pop($perguntas);
        
        //Embaralha o array de perguntas
        shuffle($perguntas);

        $perguntasGraficos = array();
        array_push($perguntasGraficos, $penultima); 
        array_push($perguntasGraficos, $ultima); 
  
        //Embaralha o array de perguntas dos graficos
        shuffle($perguntasGraficos);
        
        array_push($perguntas, $antepenultima);
        
        array_push($perguntas, array_pop($perguntasGraficos));
        array_push($perguntas, array_pop($perguntasGraficos));
    }
    //Salva na sessão
    $_SESSION['listaPerguntas'] = $perguntas;
    $_SESSION['proxPergunta'] = -1;
}

function buscarListaRespostas1($usuario) {
    //Busca as perguntas
    $queryBusca = ' SELECT CDRESPERGUNTA,
                           CDRESRESPOSTA,
                           CDRESTEXTO
                    FROM   CDRESPOSTA
                    WHERE  CDRESUSUARIO = ' . $usuario . ';';

    $resultBusca = pg_query(CONN, $queryBusca);
    $respostas = array();
    while ($row = pg_fetch_row($resultBusca)) {
        $respostas[] = $row;
    }
    //Salva na sessão
    $_SESSION['listaRespostas'] = $respostas;
}
