<?php

//Pega a conexão com o banco
require ('../bd/conexao.php');

//Busca o usuario da sessão
$usuario = $_SESSION['idUsuario'];
$origem = $_SESSION['origem'];

//Se existir usuário
if ($usuario != null) {
    buscarListaRespostas2($usuario);
    if ($origem == 'questionarioInicial') {
        deletarRespostasAnteriores($usuario, ['1', '2', '3', '4', '5', '6', '7']);
        $respostas = $_POST;
        foreach ($respostas as $pergunta => $resposta) {
            if ($pergunta[0] == 'P') {
                if ($pergunta[1] != '5') {
                    salvarRespostasUsuario($usuario, $pergunta[1], $resposta);
                } else {
                    foreach ($resposta as $respostaMultipla) {
                        salvarRespostasUsuario($usuario, $pergunta[1], $respostaMultipla);
                    }
                }
            }
        }
        require ('../view/explicacaoAvaliacao.php');
    } else if ($origem == 'explicacao' || $origem == 'login') {
        $listaRespostas = $_SESSION['listaRespostas'];
        if ($listaRespostas) {
            $tecnologiaAssistiva = Array();
            foreach ($listaRespostas as $resposta) {
                if ($resposta[0] == 1) {
                    $grauDeficiencia = $resposta[1];
                } else if ($resposta[0] == 2) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $tempoDeficiencia = '2PNR';
                    } else if ($resposta[1] == 'Desde o nascimento') {
                        $tempoDeficiencia = '2DN';
                    } else if ($resposta[1] == '5 anos') {
                        $tempoDeficiencia = '25';
                    } else if ($resposta[1] == '10 anos') {
                        $tempoDeficiencia = '210';
                    } else if ($resposta[1] == '15 anos') {
                        $tempoDeficiencia = '215';
                    } else if ($resposta[1] == '20 anos') {
                        $tempoDeficiencia = '220';
                    } else if ($resposta[1] == '25 ANOS') {
                        $tempoDeficiencia = '225';
                    } else if ($resposta[1] == 'Mais de 25 anos') {
                        $tempoDeficiencia = '2mais25';
                    }
                } else if ($resposta[0] == 3) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $escolaridade = '3PNR';
                    } else if ($resposta[1] == 'Nenhuma escolaridade') {
                        $escolaridade = '3NENHUMA';
                    } else if ($resposta[1] == 'Ensino Fundamental incompleto') {
                        $escolaridade = '3EFI';
                    } else if ($resposta[1] == 'Ensino Fundamental completo') {
                        $escolaridade = '3EFC';
                    } else if ($resposta[1] == 'Ensino Médio incompleto') {
                        $escolaridade = '3EMI';
                    } else if ($resposta[1] == 'Ensino Médio completo') {
                        $escolaridade = '3EMC   ';
                    } else if ($resposta[1] == 'Superior incompleto') {
                        $escolaridade = '3SI';
                    } else if ($resposta[1] == 'Superior completo') {
                        $escolaridade = '3SC';
                    } else if ($resposta[1] == 'Pós-graduação: especialização') {
                        $escolaridade = '3PE';
                    } else if ($resposta[1] == 'Pós-graduação: mestrado') {
                        $escolaridade = '3PM';
                    } else if ($resposta[1] == 'Pós-graduação: doutorado') {
                        $escolaridade = '3PD';
                    }
                } else if ($resposta[0] == 4) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $conhecimento = '4PNR';
                    } else if ($resposta[1] == 'Nenhum') {
                        $conhecimento = '4N';
                    } else if ($resposta[1] == 'Básico') {
                        $conhecimento = '4B';
                    } else if ($resposta[1] == 'Intermediário') {
                        $conhecimento = '4I';
                    } else if ($resposta[1] == 'Avançado') {
                        $conhecimento = '4A';
                    } else if ($resposta[1] == 'Especialista') {
                        $conhecimento = '4E';
                    }
                } else if ($resposta[0] == 5) {
                    if ($resposta[1] == 'Prefiro Não Responder') {
                        $tecnologiaAssistiva[] = '5PNR';
                    } else if ($resposta[1] == 'Nenhuma tecnologia assistiva') {
                        $tecnologiaAssistiva[] = '5N';
                    } else if ($resposta[1] == 'Leitor de tela') {
                        $tecnologiaAssistiva[] = '5LT';
                    } else if ($resposta[1] == 'Ampliador de tela') {
                        $tecnologiaAssistiva[] = '5AT';
                    } else if ($resposta[1] == 'Teclado alternativo') {
                        $tecnologiaAssistiva[] = '5TA';
                    } else if ($resposta[1] == 'Mouse alternativo') {
                        $tecnologiaAssistiva[] = '5MA';
                    } else if ($resposta[1] == 'Impressora braile') {
                        $tecnologiaAssistiva[] = '5IB';
                    } else if ($resposta[1] == 'Navegador textual') {
                        $tecnologiaAssistiva[] = '5NT';
                    } else if ($resposta[1] == 'Navegador com voz') {
                        $tecnologiaAssistiva[] = '5NV';
                    }
                } else if ($resposta[0] == 6) {
                    $importanciaFiguras = $resposta[1];
                } else if ($resposta[0] == 7) {
                    $facilidadeFiguras = $resposta[1];
                }
            }
        }
        require ('../view/questionarioInicial.php');
    }
} else {
    //Encaminha para a tela inicial
    require ('../view/telaInicial.php');
}

function salvarRespostasUsuario($usuario, $pergunta, $resposta) {
    //Salva os dados do usuario
    $insert = 'INSERT INTO CDRESPOSTA (CDRESPERGUNTA,
                                        CDRESUSUARIO,
                                        CDRESRESPOSTA)
                      VALUES (' . $pergunta . ','
            . $usuario . ''
            . ',\'' . $resposta . '\');';
    pg_query(CONN, $insert);
}

function deletarRespostasAnteriores($usuario, $perguntas) {
    foreach ($perguntas as $pergunta) {
        //Salva os dados do usuario
        $delete = ' DELETE FROM CDRESPOSTA
                WHERE  CDRESUSUARIO = ' . $usuario . ' AND
                       CDRESPERGUNTA = ' . $pergunta;
        pg_query(CONN, $delete);
    }
}

function buscarListaRespostas2($usuario) {
    //Busca as perguntas
    $queryBusca = ' SELECT CDRESPERGUNTA,
                           CDRESRESPOSTA,
                           CDRESTEXTO
                    FROM   CDRESPOSTA
                    WHERE  CDRESUSUARIO = ' . $usuario . ';';

    $resultBusca = pg_query(CONN, $queryBusca);
    $respostas = array();
    while ($row = pg_fetch_row($resultBusca)) {
        $respostas[] = $row;
    }
    //Salva na sessão
    $_SESSION['listaRespostas'] = $respostas;
}
