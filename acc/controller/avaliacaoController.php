<?php
require ('../bd/conexao.php');
if (!isset($_SESSION)) {
    session_start('questionario');
}
$usuario = $_SESSION['idUsuario'];
$origem = $_SESSION['origem'];
$botaoPressionado = isset($_GET['btnPressed']) ? $_GET['btnPressed'] : null;
if ($usuario != null) {
    buscarListaRespostas3($usuario);
    if ($origem == 'avaliacao' && $botaoPressionado != 'Voltar') {
        $idPergunta = $_SESSION['idPergunta'];
        $row = buscarRespostaAnterior($usuario, $idPergunta);
        if ($row) {
            atualizarResposta($usuario,$idPergunta);
        } else {
            salvarRespostas($usuario,$idPergunta);
        }
    }
    //Busca a imagem
    $proxPergunta = $_SESSION['proxPergunta'];
    if (isset($botaoPressionado) && $botaoPressionado == 'Voltar') {
        $proxPergunta--;
    } else {
        $proxPergunta++;
    }
    $_SESSION['proxPergunta'] = $proxPergunta;
    $perguntas = $_SESSION['listaPerguntas'];
    $pergunta = isset($perguntas[$proxPergunta]) ? $perguntas[$proxPergunta] : null;

    if (empty($pergunta)) {
        if ($proxPergunta == '-1') {
            require ('../view/explicacaoAvaliacao.php');
        } else {
            //Todas perguntas foram respondidas
            require ('../controller/questionarioFinalController.php');
        }
    } else {
        $_SESSION['idPergunta'] = $pergunta[0];
        $row = buscarRespostaAnterior($usuario, $pergunta[0]);
        if ($row) {
            $respostaAnterior = $row[0];
        }
        $imagem = $pergunta[1];
        $opc1 = $pergunta[2];
        $opc2 = $pergunta[3];
        $opc3 = $pergunta[4];
        $opc4 = $pergunta[5];
        if ($imagem == 'BARRAS' || $imagem == 'BARRASROTULO' || $imagem == 'BARRASROTULO2') {
            $centrosX = explode(';', $pergunta[6]);
            $centroX1 = $centrosX[0];
            $centroX2 = $centrosX[1];
            $centroX3 = $centrosX[2];
            $centroX4 = $centrosX[3];
            $centrosY = explode(';', $pergunta[7]);
            $centroY1 = $centrosY[0];
            $centroY2 = $centrosY[1];
            $centroY3 = $centrosY[2];
            $centroY4 = $centrosY[3];

            if ($imagem == 'BARRASROTULO'){
                $titulo1 = 'A';
                $titulo2 = 'B';
                $titulo3 = 'C';
                $titulo4 = 'D';
            }else{
                $titulo1 = 'Torre Eiffel';
                $titulo2 = 'Empire State';
                $titulo3 = 'Taipei 101';
                $titulo4 = 'Torre Burj';                
            }
        } else {
            $centroX = $pergunta[6];
            $centroY = $pergunta[7];
            $titulo = $imagem;
        }
        require ('../view/avaliacao.php');
    }
} else {
    require ('../view/telaInicial.php');
}

function buscarRespostaAnterior($usuario, $idPergunta) {
    //Busca as perguntas
    $queryBusca = ' SELECT CDRESRESPOSTA
                    FROM CDRESPOSTA
                    WHERE CDRESPERGUNTA = ' . $idPergunta . ' AND
                          CDRESUSUARIO  = ' . $usuario . ';';

    $resultBusca = pg_query(CONN, $queryBusca);
    $row = pg_fetch_row($resultBusca);
    return $row;
}

function salvarRespostas($usuario,$idPergunta) {
    //Salva os dados do usuario
    $insert = 'INSERT INTO CDRESPOSTA (CDRESPERGUNTA,
                                        CDRESUSUARIO,
                                        CDRESRESPOSTA)
                      VALUES (' . $idPergunta . ','
                              . $usuario . ''
                              . ',\'' . $_POST['figura'] . '\');';
 
    pg_query(CONN, $insert);
}

function atualizarResposta($usuario,$idPergunta){
    //Salva os dados do usuario
    $update = 'UPDATE CDRESPOSTA '
            . 'SET CDRESRESPOSTA = \''.$_POST['figura'] .'\' '
            . 'WHERE CDRESPERGUNTA = '.$idPergunta
            . '      AND CDRESUSUARIO = '. $usuario .';';

    pg_query(CONN, $update);    
}

function buscarListaRespostas3($usuario) {
    //Busca as perguntas
    $queryBusca = ' SELECT CDRESPERGUNTA,
                           CDRESRESPOSTA,
                           CDRESTEXTO
                    FROM   CDRESPOSTA
                    WHERE  CDRESUSUARIO = ' . $usuario . ';';

    $resultBusca = pg_query(CONN, $queryBusca);
    $respostas = array();
    while ($row = pg_fetch_row($resultBusca)) {
        $respostas[] = $row;
    }
    //Salva na sessão
    $_SESSION['listaRespostas'] = $respostas;
}
