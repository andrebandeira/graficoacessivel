function loadPage(page) {
    page = 'acc/'+page;
    showWait('Aguarde...');
    $.ajax({
        type: "POST",
        url: page,
        data: $("#form").serialize(),
        success: function (data)
        {
            $("#mainContent").empty();
            $("#mainContent").html(data);
            hideWait();
        },
        error: function ()
        {
            hideWait();
        }
    });
};

function showWait(text) {
    if ($('body').find('#resultLoading').prop('id') != 'resultLoading') {
        $('body').append('<div id="resultLoading" style="display:none"><div>' +
                ' <iframe src="acc/include/waiting.html" scrolling="NO" style="width:130px; height:60px;" frameborder="0"></iframe>' +
                '<div>' + text + '</div></div><div class="bg"></div></div>');
    }

    $('#resultLoading').css({
        'width': '100%',
        'height': '100%',
        'position': 'fixed',
        'z-index': '10000000',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto'
    });

    $('#resultLoading .bg').css({
        'background': '#000000',
        'opacity': '0.7',
        'width': '100%',
        'height': '100%',
        'position': 'absolute',
        'top': '0'
    });

    $('#resultLoading>div:first').css({
        'width': '250px',
        'height': '75px',
        'text-align': 'center',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto',
        'font-size': '16px',
        'z-index': '10',
        'color': '#ffffff'

    });

    $('#resultLoading .bg').height('100%');
    $('#resultLoading').fadeIn(300);
    $('body').css('cursor', 'wait');
}

function hideWait()
{
    $('#resultLoading .bg').height('100%');
    $('#resultLoading').fadeOut(300);
    $('body').css('cursor', 'default');
}

function exibeMensagem(msg) {
    alert(msg);
}

function limparQuestionario() {
    $('#form').get(0).reset();
}

function getSelectValue(id) {
    if ($('#'+id).find(":selected").val()== null || $('#'+id).find(":selected").val()==0){
        return false;
    }
    return $('#'+id).find(":selected").val();  
}

function getRadioValue(name) {
    return $('input[name='+name+']:checked', '#form').val();
}