function acessibilidade() {
    var distanciaAnterior = null;
    var audioArea = document.getElementById('audioArea');
    var audioCentro = document.getElementById('audioCentro');
    var audioBorda = document.getElementById('audioBorda');

    $('.border').mouseover(function () {
        audioBorda.volume = 0.5;
        audioBorda.play();
    });

    $('.border').mouseout(function () {
        audioBorda.pause();
    });

    $('.accessible').mouseover(function () {
        audioArea.volume = 0.5;
        audioCentro.volume = 0.5;
        audioBorda.pause();
        audioArea.play();
    });

    $('.accessible').mouseout(function () {
        audioArea.pause();
    });

    $('.accessible').mousemove(function (event) {
        verificaDistancia(this.getAttribute("centrox"), this.getAttribute("centroy"), event.pageX, event.pageY);
    });

    $('.title').mouseover(function () {
        this.setAttribute('title',this.getAttribute("description"));
        this.setAttribute('alt',this.getAttribute("description"));        
        if ($.browser.name != 'chrome'){
            $('#textoLeitor').html(this.getAttribute("description"));
            $('#textoLeitor').focus();
        }
    });
    
    $('.title').mouseout(function () {
        if ($.browser.name != 'chrome'){
            $('#textoLeitor').html('');
            $('#nada').focus();
        }
    });    

    function verificaDistancia(centroX, centroY, mouseX, mouseY) {
        var distancia = calcularDistancia(centroX, centroY, mouseX, mouseY);
        if (distancia < 10) {
            audioArea.pause();
            audioCentro.play();
        } else {
            audioCentro.pause();
            audioArea.play();
            if (distanciaAnterior === null || distancia < distanciaAnterior) {
                if (audioArea.volume < 0.98) {
                    audioArea.volume += 0.005;
                }
            } else {
                if (audioArea.volume > 0.1) {
                    audioArea.volume -= 0.005;
                }
            }
        }
        distanciaAnterior = distancia;
    }

    function calcularDistancia(centroX, centroY, mouseX, mouseY) {
        var x = centroX;
        var x0 = mouseX;
        var y = centroY;
        var y0 = mouseY;
        return Math.sqrt((x -= x0) * x + (y -= y0) * y);
    }


}

     