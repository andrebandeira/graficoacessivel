<?php
if (!isset($_SESSION)) {
    session_start('questionario');
}
$_SESSION['origem'] = 'questionarioFinal';
?>
<div class="row-fluid">
    <div class="span9 mainContent msg">
        <form id='form'>
            <p tabindex="1" class="inicio" >Considerando a sua opinião, indique o seu nível de concordância com as afirmações seguintes.</p>
            <div class = "pergunta">
                <label for = "P15">15. As tarefas propostas foram entendidas.</label><br/>
                <select tabindex="2"  id="P15" name="P15">
                    <option value="" selected=""></option>
                    <option id='15PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='15DT' value = "Discordo Totalmente">Discordo Totalmente</option>
                    <option id='15D' value = "Discordo">Discordo</option>
                    <option id='15NCND' value = "Nem Concordo Nem Discordo">Nem Concordo Nem Discordo</option>
                    <option id='15C' value = "Concordo">Concordo</option>
                    <option id='15CT' value = "Concordo Totalmente">Concordo Totalmente</option>
                </select>
            </div>
            <div class = "pergunta">
                <label for = "P16">16. As tarefas propostas forma fáceis de concluir.</label><br/>
                <select tabindex="3" id="P16" name="P16">
                    <option value="" selected=""></option>
                    <option id='16PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='16DT' value = "Discordo Totalmente">Discordo Totalmente</option>
                    <option id='16D' value = "Discordo">Discordo</option>
                    <option id='16NCND' value = "Nem Concordo Nem Discordo">Nem Concordo Nem Discordo</option>
                    <option id='16C' value = "Concordo">Concordo</option>
                    <option id='16CT' value = "Concordo Totalmente">Concordo Totalmente</option>
                </select>
            </div>   
            <div class = "pergunta">
                <label for = "P17">17. As tarefas propostas foram difíceis de concluir.</label><br/>
                <select tabindex="4" id="P17" name="P17">
                    <option value="" selected=""></option>
                    <option id='17PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='17DT' value = "Discordo Totalmente">Discordo Totalmente</option>
                    <option id='17D' value = "Discordo">Discordo</option>
                    <option id='17NCND' value = "Nem Concordo Nem Discordo">Nem Concordo Nem Discordo</option>
                    <option id='17C' value = "Concordo">Concordo</option>
                    <option id='17CT' value = "Concordo Totalmente">Concordo Totalmente</option>
                </select>
            </div> 
            <div class = "pergunta">
                <label for = "P18">18. Por meio do sistema é possível construir uma imagem mental de elementos geométricos.</label><br/>
                <select tabindex="5" id="P18" name="P18">
                    <option value="" selected=""></option>
                    <option id='18PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='18DT' value = "Discordo Totalmente">Discordo Totalmente</option>
                    <option id='18D' value = "Discordo">Discordo</option>
                    <option id='18NCND' value = "Nem Concordo Nem Discordo">Nem Concordo Nem Discordo</option>
                    <option id='18C' value = "Concordo">Concordo</option>
                    <option id='18CT' value = "Concordo Totalmente">Concordo Totalmente</option>
                </select>
            </div>  
            <div class = "pergunta">
                <label for = "P19">19. Por meio do sistema é possível construir uma imagem mental de gráficos.</label><br/>
                <select tabindex="6"  id="P19" name="P19">
                    <option value="" selected=""></option>
                    <option id='19PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='19DT' value = "Discordo Totalmente">Discordo Totalmente</option>
                    <option id='19D' value = "Discordo">Discordo</option>
                    <option id='19NCND' value = "Nem Concordo Nem Discordo">Nem Concordo Nem Discordo</option>
                    <option id='19C' value = "Concordo">Concordo</option>
                    <option id='19CT' value = "Concordo Totalmente">Concordo Totalmente</option>
                </select>
            </div>  
            <div class = "pergunta">
                <label for = "P20">20. Por meio do sistema é possível compreender a representação das informações em um gráfico de colunas (barras).</label><br/>
                <select tabindex="7"  id="P20" name="P20">
                    <option value="" selected=""></option>
                    <option id='20PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='20DT' value = "Discordo Totalmente">Discordo Totalmente</option>
                    <option id='20D' value = "Discordo">Discordo</option>
                    <option id='20NCND' value = "Nem Concordo Nem Discordo">Nem Concordo Nem Discordo</option>
                    <option id='20C' value = "Concordo">Concordo</option>
                    <option id='20CT' value = "Concordo Totalmente">Concordo Totalmente</option>
                </select>
            </div>      
            <div class = "pergunta">
                <label for = "P21">21. Quais foram as suas dificuldades no uso do sistema ?</label><br/>
                <textarea tabindex="8"  id="P21" name="P21"> <?php if (isset($dificuldades)) {
    echo $dificuldades;
} ?> </textarea>         
            </div>                   
            <div class = "pergunta">
                <label for = "P22">22. Quais as suas sugestões de melhoria para o sistema ?</label><br/>
                <textarea tabindex="9"  id="P22" name="P22"> <?php if (isset($sugestoes)) {
    echo $sugestoes;
} ?> </textarea>         
            </div>                 <div class = "pergunta">
                <label for = "P23">23. Você considera que a proposta do sistema tem potencial para auxiliar o reconhecimento e interpretação de gráficos ?</label><br/>
                <textarea tabindex="10" id="P23" name="P23"> <?php if (isset($potencial)) {
    echo $potencial;
} ?> </textarea>         
            </div>       

        </form>
        <div class = "botoes">
            <input tabindex="11" type = "button" value = "Salvar Resposta" onclick = "camposObrigatorios()"/>
            <input tabindex="12" type = "button" value = "Voltar" onclick = "loadPage('controller/avaliacaoController.php?btnPressed=Voltar');"/>
        </div>
    </div>
</div>
<script>
    function camposObrigatorios() {
        var msg = '';
        var entendimentoTarefas = getSelectValue('P15');
        var facilidadeConclusao = getSelectValue('P16');
        var dificuldadeConclusao = getSelectValue('P17');
        var imagemMental = getSelectValue('P18');
        var imagemMentalGrafico = getSelectValue('P19');
        var representacaoInformacoes = getSelectValue('P20');

        if (!entendimentoTarefas)
            msg += 'Por favor responda a pergunta 15.\n';
        if (!facilidadeConclusao)
            msg += 'Por favor responda a pergunta 16.\n';
        if (!dificuldadeConclusao)
            msg += 'Por favor responda a pergunta 17.\n';
        if (!imagemMental)
            msg += 'Por favor responda a pergunta 18.\n';
        if (!imagemMentalGrafico)
            msg += 'Por favor responda a pergunta 19.\n';
        if (!representacaoInformacoes)
            msg += 'Por favor responda a pergunta 20.\n';

        if (msg === '') {
            loadPage('controller/questionarioFinalController.php');
        } else {
            alert(msg);
        }
    }
    $(document).ready(function () {
    <?php
        if (isset($entendimentoTarefas)) {
            echo "$('#P15 option').prop('selected', false);";
            echo "$('#" . $entendimentoTarefas . "').prop('selected', true);";
        }
        if (isset($facilidadeConclusao)) {
            echo "$('#P16 option').prop('selected', false);";
            echo "$('#" . $facilidadeConclusao . "').prop('selected', true);";
        }
        if (isset($dificuldadeConclusao)) {
            echo "$('#P17 option').prop('selected', false);";
            echo "$('#" . $dificuldadeConclusao . "').prop('selected', true);";
        }
        if (isset($imagemMental)) {
            echo "$('#P18 option').prop('selected', false);";
            echo "$('#" . $imagemMental . "').prop('selected', true);";
        }
        if (isset($imagemMentalGrafico)) {
            echo "$('#P19 option').prop('selected', false);";
            echo "$('#" . $imagemMentalGrafico . "').prop('selected', true);";
        }
        if (isset($representacaoInformacoes)) {
            echo "$('#P20 option').prop('selected', false);";
            echo "$('#" . $representacaoInformacoes . "').prop('selected', true);";
        }
    ?>
        $('.inicio')[0].focus();
    });
</script>