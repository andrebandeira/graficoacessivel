<?php
if (!isset($_SESSION)) {
    session_start('questionario');
}
$_SESSION['origem'] = 'questionarioInicial';
?>
<div class="row-fluid">
    <div class="span9 mainContent msg">
        <form id='form'>
            <div>
                <p tabindex="1" class="inicio">Primeiramente muito obrigado por aceitar participar da avaliação.</p>
                <p tabindex="2">Nesta primeira parte você responderá algumas perguntas sobre suas informações pessoais e da sua experiência com acessibilidade, com a finalidade de caracterizar os participantes desta pesquisa.</p>
            </div>

            <div class="pergunta">
                <p tabindex="3">1. Qual o grau atual da sua deficiência visual?</p>
                <div class="radioOption">
                    <label for="1PNR">Prefiro Não Responder</label>
                    <input tabindex="4" type="radio" id="1PNR" name="P1" value="Prefiro Não Responder">
                </div>                
                <div class="radioOption">
                    <label for="1T">Total</label>
                    <input tabindex="4" type="radio" id="1T" name="P1" value="Total">
                </div>
                <div class="radioOption">
                    <label for = "1P">Parcial</label>
                    <input tabindex="4" type = "radio" id="1P" name = "P1" value = "Parcial" >
                </div>
            </div>

            <div class = "pergunta">
                <label for = "P2">2. Há quanto tempo possui deficiência visual?</label><br/>
                <select tabindex="5" id="P2" name="P2">
                    <option value="" selected=""></option>
                    <option id='2PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='2DN' value = "Desde o nascimento">Desde o nascimento</option>
                    <option id='25'  value = "5 anos">5 anos</option>
                    <option id='210' value = "10 anos">10 anos</option>
                    <option id='215' value = "15 anos">15 anos</option>
                    <option id='220' value = "20 anos">20 anos</option>
                    <option id='225' value = "25 anos">25 anos</option>
                    <option id='2mais25' value = "Mais de 25 anos">Mais de 25 anos</option>
                </select>
            </div>

            <div class = "pergunta">
                <label for = "P3">3. Qual a sua escolaridade?</label><br/>
                <select tabindex="6" id = "P3" name = "P3">
                    <option value="" selected=""></option>
                    <option id='3PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='3NENHUMA' value = "Nenhuma escolaridade">Nenhuma escolaridade</option>
                    <option id='3EFI' value = "Ensino Fundamental incompleto">Ensino Fundamental incompleto</option>
                    <option id='3EFC' value = "Ensino Fundamental completo">Ensino Fundamental completo</option>
                    <option id='3EMI' value = "Ensino Médio incompleto">Ensino Médio incompleto</option>
                    <option id='3EMC' value = "Ensino Médio completo">Ensino Médio completo</option>
                    <option id='3SI' value = "Superior incompleto">Superior incompleto</option>
                    <option id='3SC' value = "Superior completo">Superior completo</option>
                    <option id='3PE' value = "Pós-graduação: especialização">Pós-graduação: especialização</option>
                    <option id='3PM' value = "Pós-graduação: mestrado">Pós-graduação: mestrado</option>
                    <option id='3PD' value = "Pós-graduação: doutorado">Pós-graduação: doutorado</option>
                </select>
            </div>

            <div class = "pergunta">
                <label for = "P4">4. Como você classifica seu conhecimento como usuário de computadores?</label><br/>
                <select tabindex="7"  id = "P4" name = "P4">
                    <option value="" selected=""></option>
                    <option id='4PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='4N' value = "Nenhum">Nenhum</option>
                    <option id='4B' value = "Básico">Básico</option>
                    <option id='4I' value = "Intermediário">Intermediário</option>
                    <option id='4A' value = "Avançado">Avançado</option>
                    <option id='4E' value = "Especialista">Especialista</option>
                </select>
            </div>

            <div class = "pergunta">
                <label for = "P5">5. Qual tecnologia assistiva você utiliza com frequência? (resposta múltipla)</label><br/>
                <select tabindex="8"  id = "P5" name = "P5[]" multiple="">
                    <option id='5PNR' value="Prefiro Não Responder">Prefiro Não Responder</option>
                    <option id='5N'  value = "Nenhuma tecnologia assistiva">Nenhuma tecnologia assistiva</option>
                    <option id='5LT' value = "Leitor de tela">Leitor de tela</option>
                    <option id='5AT' value = "Ampliador de tela">Ampliador de tela</option>
                    <option id='5TA' value = "Teclado alternativo">Teclado alternativo</option>
                    <option id='5MA' value = "Mouse alternativo">Mouse alternativo</option>
                    <option id='5IB' value = "Impressora braile">Impressora braile</option>
                    <option id='5NT' value = "Navegador textual">Navegador textual</option>
                    <option id='5NV' value = "Navegador com voz">Navegador com voz</option>
                </select>
            </div>

            <div class = "pergunta">
                <p tabindex="9">6. Qual a importância de figuras e gráficos no seu dia a dia?</p>
                <div class="radioOption">
                    <div class="radioOption">
                        <label for="6PNR">Prefiro Não Responder</label>
                        <input tabindex="10" type="radio" id="6PNR" name="P6" value="Prefiro Não Responder">
                    </div>                        
                    <label for = "6N">Nenhuma</label>
                    <input tabindex="10"  type = "radio" id = "6N" name = "P6" value = "Nenhuma">
                </div>
                <div class="radioOption">
                    <label for = "6M">Média</label>
                    <input tabindex="10"  type = "radio" id = "6M" name = "P6" value = "Média">
                </div>
                <div class="radioOption">
                    <label for = "6A">Alta</label>
                    <input tabindex="10"  type = "radio" id = "6A" name = "P6" value = "Alta">
                </div>
            </div>

            <div class = "pergunta">
                <p tabindex="11">7. Qual a facilidade de entendimento de figuras e gráficos utilizando tecnologias assistivas?</p>
                <div class="radioOption">
                    <label for="7PNR">Prefiro Não Responder</label>
                    <input tabindex="12" type="radio" id="7PNR" name="P7" value="Prefiro Não Responder">
                </div>                   
                <div class="radioOption">
                    <label for = "7N">Nenhuma</label>
                    <input tabindex="12" type = "radio" id = "7N" name = "P7" value = "Nenhuma">
                </div>
                <div class="radioOption">
                    <label for = "7M">Média</label>
                    <input tabindex="12" type = "radio" id = "7M" name = "P7" value = "Média">
                </div>
                <div class="radioOption">
                    <label for = "7A">Alta</label>
                    <input tabindex="12" type = "radio" id = "7A" name = "P7" value = "Alta">
                </div>
            </div>
        </form>
        <div class = "botoes">
            <input tabindex="13" type = "button" value = "Salvar Resposta" onclick = "camposObrigatorios()"/>
            <input tabindex="14" type = "button" value = "Voltar" onclick = "loadPage('view/telaInicial.php');"/>
        </div>
    </div>
</div>
<script>
    function camposObrigatorios() {
        var msg = '';
        var grauDeficiencia = getRadioValue('P1');
        var tempoDeficiencia = getSelectValue('P2');
        var escolaridade = getSelectValue('P3');
        var conhecimento = getSelectValue('P4');
        var tecnologiaAssistiva = getSelectValue('P5');
        var importanciaFiguras = getRadioValue('P6');
        var facilidadeFiguras = getRadioValue('P7');

        if (!grauDeficiencia)
            msg += 'Por favor responda a pergunta 1.\n';
        if (!tempoDeficiencia)
            msg += 'Por favor responda a pergunta 2.\n';
        if (!escolaridade)
            msg += 'Por favor responda a pergunta 3.\n';
        if (!conhecimento)
            msg += 'Por favor responda a pergunta 4.\n';
        if (!tecnologiaAssistiva)
            msg += 'Por favor responda a pergunta 5.\n';
        if (!importanciaFiguras)
            msg += 'Por favor responda a pergunta 6.\n';
        if (!facilidadeFiguras)
            msg += 'Por favor responda a pergunta 7.\n';

        if (msg === '') {
            loadPage('controller/questionarioInicialController.php');
        } else {
            alert(msg);
        }
    }
    $(document).ready(function () {
<?php
if (isset($grauDeficiencia)) {
    if ($grauDeficiencia == 'Total') {
        echo "$('#1T').prop('checked', true);";
        echo "$('#1P').prop('checked', false);";
        echo "$('#1PNR').prop('checked', false);";
    } else if ($grauDeficiencia == 'Parcial') {
        echo "$('#1T').prop('checked', false);";
        echo "$('#1P').prop('checked', true);";
        echo "$('#1PNR').prop('checked', false);";
    } else if ($grauDeficiencia == 'Prefiro Não Responder') {
        echo "$('#1T').prop('checked', false);";
        echo "$('#1P').prop('checked', false);";
        echo "$('#1PNR').prop('checked', true);";
    }
}

if (isset($importanciaFiguras)) {
    if ($importanciaFiguras == 'Nenhuma') {
        echo "$('#6N').prop('checked', true);";
        echo "$('#6M').prop('checked', false);";
        echo "$('#6A').prop('checked', false);";
        echo "$('#6PNR').prop('checked', false);";
    } else if ($importanciaFiguras == 'Média') {
        echo "$('#6N').prop('checked', false);";
        echo "$('#6M').prop('checked', true);";
        echo "$('#6A').prop('checked', false);";
        echo "$('#6PNR').prop('checked', false);";
    } else if ($importanciaFiguras == 'Alta') {
        echo "$('#6N').prop('checked', false);";
        echo "$('#6M').prop('checked', false);";
        echo "$('#6A').prop('checked', true);";
        echo "$('#6PNR').prop('checked', false);";
    }else if ($importanciaFiguras == 'Prefiro Não Responder') {
        echo "$('#6N').prop('checked', false);";
        echo "$('#6M').prop('checked', false);";
        echo "$('#6A').prop('checked', false);";
        echo "$('#6PNR').prop('checked', true);";
    }
}

if (isset($facilidadeFiguras)) {
    if ($facilidadeFiguras == ' Nenhuma') {
        echo "$('#7N').prop('checked', true);";
        echo "$('#7M').prop('checked', false);";
        echo "$('#7A').prop('checked', false);";
        echo "$('#7PNR').prop('checked', false);";
    } elseif ($facilidadeFiguras == 'Média') {
        echo "$('#7N').prop('checked', false);";
        echo "$('#7M').prop('checked', true);";
        echo "$('#7A').prop('checked', false);";
        echo "$('#7PNR').prop('checked', false);";
    } elseif ($facilidadeFiguras == 'Alta') {
        echo "$('#7N').prop('checked', false);";
        echo "$('#7M').prop('checked', false);";
        echo "$('#7A').prop('checked', true);";
        echo "$('#7PNR').prop('checked', false);";
    } else if ($facilidadeFiguras == 'Prefiro Não Responder') {
        echo "$('#7N').prop('checked', false);";
        echo "$('#7M').prop('checked', false);";
        echo "$('#7A').prop('checked', false);";
        echo "$('#7PNR').prop('checked', true);";
    }
}

if (isset($tempoDeficiencia)) {
    echo "$('#P2 option').prop('selected', false);";
    echo "$('#" . $tempoDeficiencia . "').prop('selected', true);";
}

if (isset($escolaridade)) {
    echo "$('#P3 option').prop('selected', false);";
    echo "$('#" . $escolaridade . "').prop('selected', true);";
}
if (isset($conhecimento)) {
    echo "$('#P4 option').prop('selected', false);";
    echo "$('#" . $conhecimento . "').prop('selected', true);";
}
if (isset($tecnologiaAssistiva)) {
    echo "$('#P5 option').prop('selected', false);";
    foreach ($tecnologiaAssistiva as $tecnologia) {
        echo "$('#" . $tecnologia . "').prop('selected', true);";
    }
}
?>
        $('.inicio')[0].focus();

    });
</script>