<div class="row-fluid">
    <div class="span9 mainContent msg">       
        <br/>
        <form id="form">
            <input type="hidden" id="TentativaAnterior" name="TentativaAnterior"/>
            <div>
                <p tabindex="1" class="inicio">Já existe uma avaliação com o seu IP. Se você já respondeu este questionário, podemos recuperar suas respostas para facilitar quaisquer mudanças.</p>
                <p tabindex="2">Deseja continuar a avaliação anterior ou começar uma nova?</p>
            </div>
            <div class="botoes">
                <input tabindex="3" type="button" value="Continuar Avaliação Anterior" onclick="enviarOpcao('SIM')"/>
                <input tabindex="4" type="button" value="Iniciar Nova Avaliação" onclick="enviarOpcao('NAO')"/>
            </div>
        </form>
    </div>
</div>
<script>
    function enviarOpcao(opcao) {
        $('#TentativaAnterior').val(opcao);
        loadPage('controller/loginController.php');
    }
    $(document).ready(function () {
        $('.inicio')[0].focus();
    });
</script>
