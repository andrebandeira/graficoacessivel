<?php
if (!isset($_SESSION)) {
    session_start('questionario');
}
$_SESSION['origem'] = 'explicacao';
?>
<div class="row-fluid">
    <div class="span9 mainContent msg">       
        <br/>
        <form id="form">
            <div>
                <p tabindex="1" class="inicio">
                    Nesta segunda parte você interagirá com o sistema por meio do mouse, ouvindo um retorno sonoro de acordo com o local da figura correspondente à posição do mouse. 
                </p>
                <p tabindex="2">
                    Os valores referentes à posição do mouse na figura são tratados e separados em três áreas diferentes:
                </p>
                <p tabindex="3">
                    1) As bordas do elemento geométrico, para que o usuário identiﬁque que está saindo ou entrando em um elemento.
                </p>
                <p  tabindex="4">
                    2) A área interna do elemento geométrico, de maneira que, se o usuário estiver movimentando o mouse dentro do elemento e estiver se afastando do centro o som irá diminuindo para indicar a proximidade da borda do elemento.
                </p>
                <p  tabindex="5">
                    3) O centro do elemento geométrico. 

                </p>
            </div>
            <div class="botoes">
                <p  tabindex="6">
                    Utilize os botões seguintes para conhecer os sons utilizados no sistema em cada uma das três áreas.
                </p>
                <input  tabindex="7" id='AudioBorda' type="button" value="Ouvir Áudio Borda"/>
                <input  tabindex="8" id='AudioArea' type="button" value="Ouvir Áudio Área"/>
                <input  tabindex="9" id='AudioCentro'  type="button" value="Ouvir Áudio Centro"/>
            </div>
            </br>
            <div tabindex="10">
                <p>
                    Ao clicar no botão continuar, você responderá 7 perguntas objetivas. Em cada pergunta haverá um elemento geométrico ou um gráfico e você deverá explorar o elemento ou gráfico com o mouse e depois selecionar a opção que melhor descreve o seu reconhecimento.
                </p>
                <br/>
            </div>
            <div class="botoes">
                <input tabindex="11" type="button" value="Continuar" onclick="loadPage('controller/avaliacaoController.php');"/>
                <input tabindex="12" type="button" value="Voltar" onclick="loadPage('controller/questionarioInicialController.php');"/>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        var audioArea = document.getElementById('audioArea');
        var audioCentro = document.getElementById('audioCentro');
        var audioBorda = document.getElementById('audioBorda');
        audioBorda.volume = 0.5;
        audioCentro.volume = 0.5;
        audioArea.volume = 0.5;
        $('#AudioBorda').click(function () {
            audioBorda.play();
            this.focus();
        });
        $('#AudioBorda').focusout(function () {
            audioBorda.pause();
        });
        $('#AudioCentro').click(function () {
            audioCentro.play();
            this.focus();
        });
        $('#AudioCentro').focusout(function () {
            audioCentro.pause();
        });
        $('#AudioArea').click(function () {
            audioArea.play();
            this.focus();
        });
        $('#AudioArea').focusout(function () {
            audioArea.pause();
        });
        $('.inicio')[0].focus();
    });
</script>