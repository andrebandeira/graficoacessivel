<div class="row-fluid">
    <div class="span9 mainContent msg">       
        <br/>
        <form id="form">
            <input type="hidden" id="meuIP" name="meuIP"/>
            <div class="inicio" tabindex="1">
                <p>Bem-vindo à Avaliação de Usabilidade com Usuários Cegos de Sistema para Acesso à Gráficos</p>
                <br/>
            </div>
            <div tabindex="2">
                <p>
                    O objetivo principal desta avaliação é identificar a usabilidade de um sistema para acesso a diagramas por usuários cegos. Neste sistema é utilizado o auxílio de áudio contínuo cujo objetivo é possibilitar a construção de uma imagem mental do gráfico de maneira semelhante à leitura tátil.   
                </p>
                <p>
                    A participação do respondente neste questionário permite que o pesquisador utilize as suas respostas para realizar uma análise estatística. A participação não é obrigatória, pois o respondente tem a liberdade de se recusar a participar. O respondente poderá pedir mais informações sobre a pesquisa através dos e-mails dos pesquisadores responsáveis (apresentados no final da pesquisa).
                </p>
                <p>
                    O nome do respondente é mantido em sigilo e todas as ligações entre o respondente e seus dados serão destruídas após a análise estatística. Portanto, todos os resultados serão utilizados de maneira anônima.
                </p>
                <p>
                    O tempo médio de resposta é de 20 minutos.
                </p>
                <br/>
            </div>
            <div tabindex="3">
                <p>
                    <b>Termo de Consentimento Livre e Esclarecido</b>
                </p>
                <p>
                    Acredito estar informado, ficando claro que a minha participação é voluntária. Estou ciente dos objetivos da pesquisa, da forma como será realizada, da garantia de confidencialidade e da possibilidade de solicitar esclarecimentos. Diante do exposto, expresso minha concordância de espontânea vontade em participar desta pesquisa.
                </p>
            </div>

            <div class="botoes">
                <input tabindex="4" type="button" value="Aceito participar" onclick="loadPage('controller/loginController.php');"/>
                <input tabindex="5" type="button" value="Não Aceito participar" onclick="exibeMensagem('Sentimos muito. Obrigado pela Atenção.')"/>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#meuIP').val(myIP());
        $('.inicio')[0].focus();
    });

    function myIP() {
        if (window.XMLHttpRequest)
            xmlhttp = new XMLHttpRequest();
        else
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

        xmlhttp.open("GET", "http://api.hostip.info/get_html.php", false);
        xmlhttp.send();

        hostipInfo = xmlhttp.responseText.split("\n");

        for (i = 0; hostipInfo.length >= i; i++) {
            ipAddress = hostipInfo[i].split(":");
            if (ipAddress[0] == "IP")
                return ipAddress[1];
        }

        return false;
    }
</script>
