<?php
if (!isset($_SESSION)) {
    session_start('questionario');
}
$_SESSION['origem'] = 'avaliacao';
?>
<div id="nada" tabindex="9"></div>
<p id="textoLeitor" tabindex="8" role="alert" style="color: transparent;"></p>
<div class="row-fluid">
    <div class="span9 mainContent msg">    
        <div id="imagem">
            <svg  x='100' y='1100'width="640" height="480" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                <?php if ($imagem == 'CIRCULO') { ?>
                    <ellipse id="svg_7" class="border" fill-opacity="0" ry="155" rx="155" cy="222.4" cx="302" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="9" stroke="#000000" fill="#000000"/>
                    <ellipse d='img' name="img" class="accessible" centroX="<?php echo $centroX ?>" centroY="<?php echo $centroY ?>" description="<?php echo $titulo; ?>" stroke="#0026ff" ry="148.5" rx="148.5" cy="222.5" cx="302.5" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" fill="#0026ff" class="accessible"/>
                <?php } else if ($imagem == 'QUADRADO') { ?> ?>
                    <rect stroke="#666666" id='img' name="img" class="accessible" centroX="<?php echo $centroX ?>" centroY="<?php echo $centroY ?>" description="<?php echo $titulo; ?>" height="348" width="349.99999" y="69" x="145.00001" stroke-width="5" fill="#666666"/>
                    <line stroke="#000000" fill="none" stroke-width="8" x1="135.70096" y1="62.49999" x2="505.29906" y2="62.49999" class="border" id="svg_3"/>
                    <line stroke="#000000" id="svg_1" fill="none" stroke-width="8" x1="136.20096" y1="423.50001" x2="504.79904" y2="423.50001" class="border"/>
                    <line stroke="#000000" transform="rotate(90 500.9999694824219,242.00000000000006) " id="svg_2" fill="none" stroke-width="8" x1="317.70093" y1="242.00001" x2="684.29902" y2="242.00001" class="border"/>
                    <line stroke="#000000" id="svg_4" transform="rotate(90 139.00000000000003,242.5) " fill="none" stroke-width="8" x1="-44.79905" y1="242.50001" x2="322.79905" y2="242.50001" class="border"/>                
                <?php } else if ($imagem == 'RETANGULO') { ?> ?>
                    <rect id='img' name="img" class="accessible" centroX="<?php echo $centroX ?>" centroY="<?php echo $centroY ?>" description="<?php echo $titulo; ?>" stroke="#ff0000" height="211.00002" width="426.99999" y="72" x="98.00002" stroke-width="5" fill="#FF0000"/>
                    <line stroke="#000000" class="border" y2="288.94198" x2="527.69856" y1="288.94198" x1="93.10047" stroke-width="8" fill="none" id="svg_2"/>
                    <line stroke="#000000" transform="rotate(90 531.0000000000001,179.50000000000006) " id="svg_1" class="border" y2="179.49999" x2="644.29904" y1="179.49999" x1="417.70096" stroke-width="8" fill="none"/>
                    <line stroke="#000000" id="svg_3" class="border" y2="65.49999" x2="535.29905" y1="65.49999" x1="93.70096" stroke-width="8" fill="none"/>
                    <line stroke="#000000" id="svg_4" transform="rotate(90 91.99999999999999,177.0000152587891) " class="border" y2="177.00001" x2="207.79905" y1="177.00001" x1="-23.79905" stroke-width="8" fill="none"/>
                <?php } else if ($imagem == 'TRIANGULO') { ?> ?>
                    <polygon id='img' name="img" class="accessible" centroX="<?php echo $centroX ?>" centroY="<?php echo $centroY ?>" description="<?php echo $titulo; ?>" fill="#00007f" points="450.7467956542969,221.00003051757812 148.87661743164062,395.86175537109375 148.87661743164062,46.13827133178711 450.7467956542969,221.00003051757812 " stroke-width="10" transform="rotate(-89.99999237060547 299.8117065429687,220.99999999999997) "/>
                    <line stroke="#000000" fill="none" stroke-width="8" x1="117.99999" y1="375.5" x2="477.99999" y2="375.5" id="svg_6" class="border"/>
                    <line stroke="#000000" transform="rotate(60 389.825958251953,219.73443603515628) " id="svg_1" fill="none" stroke-width="8" x1="205.47786" y1="219.73444" x2="574.17405" y2="219.73444" class="border"/>
                    <line stroke="#000000" id="svg_2" transform="rotate(120 207.89952087402347,221.94198608398438) " fill="none" stroke-width="8" x1="26.10049" y1="221.94198" x2="389.69856" y2="221.94198" class="border"/>
                <?php } else if ($imagem == 'BARRAS') { ?>
                    <rect stroke="#ff0000" id='img1' name="img1" class="accessible" centroX="<?php echo $centroX1 ?>" centroY="<?php echo $centroY1 ?>" description="<?php echo $titulo1; ?>" height="227.99999" width="55" y="206.00001" x="114" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" fill="#FF0000"/>
                    <rect stroke="#ffff00" id='img2' name="img2" class="accessible" centroX="<?php echo $centroX2 ?>" centroY="<?php echo $centroY2 ?>" description="<?php echo $titulo2; ?>" height="369.00002" width="55" y="63.99999" x="225.5" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" fill="#ffff00"/>
                    <rect stroke="#0000ff" id='img3' name="img3" class="accessible" centroX="<?php echo $centroX3 ?>" centroY="<?php echo $centroY3 ?>" description="<?php echo $titulo3; ?>" height="126" width="53" y="307" x="334.5" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" fill="#0000ff"/>
                    <rect stroke="#007f00" id='img4' name="img4" class="accessible" centroX="<?php echo $centroX4 ?>" centroY="<?php echo $centroY4 ?>" description="<?php echo $titulo4; ?>" height="292.99999" width="55" y="140" x="443.5" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" fill="#007f00"/>
                    <line id="svg_9" y2="446" x2="47" y1="38.99999" x1="49" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" fill="none" stroke="#000000"/>
                    <line id="svg_10" y2="445" x2="579" y1="445" x1="44" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" stroke="#000000" fill="none"/>
                    <line stroke="#000000" fill="none" stroke-width="8" x1="55.981" y1="319.49999" x2="294.01899" y2="319.49999" transform="rotate(90 174.99999999999997,319.50000000000006) " id="svg_20" class="border"/>
                    <line stroke="#000000" id="svg_1" fill="none" stroke-width="8" x1="-10.519" y1="319.99999" x2="226.51898" y2="319.99999" transform="rotate(90 107.99999999999996,320.00000000000006) " class="border"/>
                    <line stroke="#000000" id="svg_2" fill="none" stroke-width="8" x1="28.48102" y1="246.00001" x2="409.51901" y2="246.00001" transform="rotate(90 219.0000152587891,246) " class="border"/>
                    <line stroke="#000000" id="svg_3" fill="none" stroke-width="8" x1="93.98102" y1="249.99999" x2="480.01902" y2="249.99999" transform="rotate(90 287.00000000000006,249.99998474121097) " class="border"/>
                    <line stroke="#000000" id="svg_4" fill="none" stroke-width="8" x1="258.98101" y1="370.00001" x2="397.01898" y2="370.00001" transform="rotate(90 328,370.00000000000006) " class="border"/>
                    <line id="svg_5" stroke="#000000" fill="none" stroke-width="8" x1="324.98101" y1="368.50001" x2="463.01898" y2="368.50001" transform="rotate(90 394,368.50000000000006) " class="border"/>
                    <line stroke="#000000" id="svg_6" fill="none" stroke-width="8" x1="282.981" y1="283.50001" x2="591.01898" y2="283.50001" transform="rotate(90 437,283.50000000000006) " class="border"/>
                    <line stroke="#000000" id="svg_7" fill="none" stroke-width="8" x1="351.981" y1="285.50001" x2="658.01898" y2="285.50001" transform="rotate(90 505,285.50000000000006) " class="border"/>
                    <line stroke="#000000" id="svg_8" fill="none" stroke-width="8" x1="104.48101" y1="200.49999" x2="178.51898" y2="200.49999" class="border"/>
                    <line stroke="#000000" id="svg_11" fill="none" stroke-width="8" x1="214.98101" y1="56.50001" x2="291.01898" y2="56.50001" class="border"/>
                    <line id="svg_12" stroke="#000000" fill="none" stroke-width="8" x1="323.98101" y1="300.50001" x2="398.01898" y2="300.50001" class="border"/>
                    <line id="svg_13" stroke="#000000" fill="none" stroke-width="8" x1="434.98101" y1="133.50001" x2="509.01898" y2="133.50001" class="border"/>
                    <line stroke="#000000" id="svg_14" fill="none" stroke-width="8" x1="214.98101" y1="439.50001" x2="289.01898" y2="439.50001" class="border"/>
                    <line stroke="#000000" id="svg_15" fill="none" stroke-width="8" x1="103.98101" y1="440.50001" x2="179.01898" y2="440.50001" class="border"/>
                    <line id="svg_16" stroke="#000000" fill="none" stroke-width="8" x1="323.98101" y1="439.50001" x2="398.01898" y2="439.50001" class="border"/>
                    <line stroke="#000000" id="svg_17" fill="none" stroke-width="8" x1="432.98101" y1="439.50001" x2="509.01898" y2="439.50001" class="border"/>            
                <?php } else if ($imagem == 'BARRASROTULO') { ?>
                    <rect fill="#00007f" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x="74" y="238" width="86" height="173" stroke="#00007f" class="accessible title"  centroX="<?php echo $centroX1 ?>" centroY="<?php echo $centroY1 ?>" description="<?php echo $titulo1; ?>" id="img1"/>
                    <rect fill="#00007f" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x="193.99999" y="103.99999" width="86.00001" height="307.00001" stroke="#00007f"  centroX="<?php echo $centroX2 ?>" centroY="<?php echo $centroY2 ?>" description="<?php echo $titulo2; ?>" class="accessible title" id="img2"/>
                    <rect fill="#00007f" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x="321" y="301" width="86" height="109" stroke="#00007f" class="accessible title" centroX="<?php echo $centroX3 ?>" centroY="<?php echo $centroY3 ?>" description="<?php echo $titulo3; ?>"  id="img3"/>
                    <rect fill="#00007f" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x="440" y="269" width="86" height="141" stroke="#00007f" class="accessible title" centroX="<?php echo $centroX4 ?>" centroY="<?php echo $centroY4 ?>" description="<?php echo $titulo4; ?>"  id="img4"/>
                    <text fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="117" y="447" id="svg_14" font-size="24" font-family="serif" text-anchor="middle" xml:space="preserve">A</text>
                    <text fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="236" y="447" id="svg_15" font-size="24" font-family="serif" text-anchor="middle" xml:space="preserve">B</text>
                    <text fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="363" y="447" id="svg_16" font-size="24" font-family="serif" text-anchor="middle" xml:space="preserve">C</text>
                    <text fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="484" y="447" id="svg_17" font-size="24" font-family="serif" text-anchor="middle" xml:space="preserve">D</text>
                    <line fill="none" stroke-width="5" stroke-dasharray="null" stroke-linejoin="null" stroke-linecap="null" x1="49" y1="42.99999" x2="47" y2="420" id="svg_9" stroke="#000000" transform="rotate(-0.5495829582214355 48,231.49998474121082) "/>
                    <line fill="none" stroke-width="5" stroke-dasharray="null" stroke-linejoin="null" stroke-linecap="null" x1="46.00001" y1="422" x2="571" y2="422" id="svg_10" stroke="#000000"/>
                    <line fill="none" stroke-width="8" x1="190" y1="97" x2="284" y2="97" id="svg_2" stroke="#000000"/>
                    <line fill="none" stroke-width="8" x1="70" y1="230.5" x2="164" y2="230.5" stroke="#000000" id="svg_5" class="border"/>
                    <line fill="none" stroke-width="8" x1="318" y1="293.5" x2="412" y2="293.5" stroke="#000000" id="svg_6" class="border"/>
                    <line fill="none" stroke-width="8" x1="437" y1="261.5" x2="531" y2="261.5" stroke="#000000" id="svg_7" class="border"/>
                    <line fill="none" stroke-width="8" x1="121.6829" y1="257.51687" x2="449.72087" y2="257.51687" id="svg_8" transform="rotate(90 285.701904296875,257.5168762207032) " stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="68.48101" y1="324.99999" x2="265.51898" y2="324.99999" transform="rotate(90 166.99999999999997,325.00000000000006) " id="svg_18" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="348.48101" y1="354.99999" x2="479.51898" y2="354.99999" transform="rotate(90 414,355.00000000000006) " id="svg_19" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="450.98102" y1="339.49996" x2="615.01901" y2="339.49996" transform="rotate(90 533.0000000000001,339.49996948242193) " id="svg_20" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="349.98102" y1="340.49996" x2="516.01901" y2="340.49996" transform="rotate(90 433,340.49996948242193) " id="svg_21" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="248.48101" y1="354.99999" x2="379.51898" y2="354.99999" transform="rotate(90 314,355.00000000000006) " id="svg_22" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="24.481" y1="256.99999" x2="351.51898" y2="256.99999" transform="rotate(90 187.99999999999997,257.00000000000006) " id="svg_23" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="-30.01897" y1="323.49996" x2="164.019" y2="323.49996" transform="rotate(90 67.00001525878902,323.49996948242193) " id="svg_24" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="70" y1="418.5" x2="164" y2="418.5" id="svg_25" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="192" y1="418.5" x2="286" y2="418.5" stroke="#000000" id="svg_26" class="border"/>
                    <line fill="none" stroke-width="8" x1="316" y1="417.5" x2="410" y2="417.5" stroke="#000000" id="svg_27" class="border"/>
                    <line fill="none" stroke-width="8" x1="437" y1="416.5" x2="531" y2="416.5" stroke="#000000" id="svg_28" class="border"/>
                <?php } else if ($imagem == 'BARRASROTULO2') { ?>    
                    <rect centroX="<?php echo $centroX1 ?>" centroY="<?php echo $centroY1 ?>" description="<?php echo $titulo1; ?>" fill="#00bf00" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x="138" y="318" width="56" height="100" stroke="#00bf00" class="accessible title" id="img1"/>
                    <rect centroX="<?php echo $centroX2 ?>" centroY="<?php echo $centroY2 ?>" description="<?php echo $titulo2; ?>" id="svg_1" fill="#ff0000" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x="244.25" y="243.5" width="56" height="173" stroke="#ff0000" class="accessible title"/>
                    <rect centroX="<?php echo $centroX3 ?>" centroY="<?php echo $centroY3 ?>" description="<?php echo $titulo3; ?>" id="svg_6" fill="#999999" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x="343.25" y="198.49999" width="56" height="220.00001" stroke="#999999" class="accessible title"/>
                    <rect centroX="<?php echo $centroX4 ?>" centroY="<?php echo $centroY4 ?>" description="<?php echo $titulo4; ?>" id="svg_12" fill="#00007f" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x="449.25" y="44.49999" width="56" height="373.00001" stroke="#00007f" class="accessible title"/>
                    <text fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="274" y="445" id="svg_15" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">Empire State</text>
                    <text fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="374" y="445" id="svg_16" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">Taipei 101</text>
                    <line fill="none" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x1="113.86567" y1="19.99235" x2="111.86567" y2="425.00064" id="svg_9" stroke="#000000" transform="rotate(-0.5495829582214355 112.86566925049529,222.49649047851528) "/>
                    <line fill="none" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" x1="111.00001" y1="427" x2="584.00001" y2="427" id="svg_10" stroke="#000000"/>
                    <line fill="none" stroke-width="8" x1="134" y1="311.5" x2="198" y2="311.5" stroke="#000000" id="svg_5" class="border"/>
                    <line fill="none" stroke-width="8" x1="135" y1="424.5" x2="199" y2="424.5" id="svg_25" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="72.98101" y1="366.49996" x2="191.019" y2="366.49996" transform="rotate(90 132.00001525878903,366.49996948242193) " id="svg_24" stroke="#000000" class="border"/>
                    <line fill="none" stroke-width="8" x1="139.48101" y1="367.99999" x2="260.51898" y2="367.99999" transform="rotate(90 199.99999999999997,368.00000000000006) " id="svg_18" stroke="#000000" class="border"/>
                    <line id="svg_2" fill="none" stroke-width="8" x1="240.25" y1="237" x2="304.25" y2="237" stroke="#000000" class="border"/>
                    <line id="svg_3" fill="none" stroke-width="8" x1="141.73102" y1="329.49996" x2="334.76899" y2="329.49996" transform="rotate(90 238.25001525878903,329.49996948242193) " stroke="#000000" class="border"/>
                    <line id="svg_4" fill="none" stroke-width="8" x1="209.23101" y1="329.99999" x2="403.26898" y2="329.99999" transform="rotate(90 306.25,330.00000000000006) " stroke="#000000" class="border"/>
                    <line id="svg_7" fill="none" stroke-width="8" x1="339.25" y1="192" x2="403.25" y2="192" stroke="#000000" class="border"/>
                    <line id="svg_8" fill="none" stroke-width="8" x1="219.73101" y1="307.49996" x2="456.76899" y2="307.49996" transform="rotate(90 338.25,307.49996948242193) " stroke="#000000" class="border"/>
                    <line id="svg_11" fill="none" stroke-width="8" x1="284.731" y1="308.49999" x2="525.76898" y2="308.49999" transform="rotate(90 405.25,308.50000000000006) " stroke="#000000" class="border"/>
                    <line id="svg_13" fill="none" stroke-width="8" x1="444.25" y1="38" x2="508.25" y2="38" stroke="#000000" class="border"/>
                    <line id="svg_19" fill="none" stroke-width="8" x1="247.73102" y1="229.49994" x2="638.76903" y2="229.49994" transform="rotate(90 443.25000000000006,229.49995422363284) " stroke="#000000" class="border"/>
                    <line id="svg_20" fill="none" stroke-width="8" x1="314.23098" y1="230.99999" x2="708.26898" y2="230.99999" transform="rotate(90 511.25,231.00000000000006) " stroke="#000000" class="border"/>
                    <line id="svg_27" fill="none" stroke-width="8" x1="241" y1="422" x2="305" y2="422" stroke="#000000" class="border"/>
                    <line id="svg_28" fill="none" stroke-width="8" x1="339" y1="423" x2="403" y2="423" stroke="#000000" class="border"/>
                    <line id="svg_30" fill="none" stroke-width="8" x1="445" y1="423" x2="513" y2="423" stroke="#000000" class="border"/>
                    <text id="svg_31" fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="475" y="446" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">Torre Burj</text>
                    <text id="svg_32" fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="164" y="445" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">Torre Eiffel</text>
                    <text id="svg_33" fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="88" y="316" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">200m</text>
                    <text id="svg_35" fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="88" y="241" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">400m</text>
                    <text id="svg_36" fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="88" y="193" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">450m</text>
                    <text id="svg_37" fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="88" y="133" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">500m</text>
                    <text id="svg_38" fill="#000000" stroke="#000000" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" x="88" y="48" font-size="18" font-family="serif" text-anchor="middle" xml:space="preserve">700m</text>
                <?php } ?>
            </svg>
        </div>
        <form id="form">
            <div class="pergunta">
                <?php
                if ($imagem == 'CIRCULO' || $imagem == 'QUADRADO' || $imagem == 'TRIANGULO' || $imagem == 'RETANGULO') {
                    echo '<p tabindex="1" class="inicio">' . (8 + $proxPergunta) . '. Utilize o mouse e o retorno de áudio para identificar o elemento geométrico e depois selecione a alternativa que corresponde ao elemento geométrico.</p>';
                } else if ($imagem == 'BARRAS') {
                    echo '<p tabindex="1" class="inicio">' . (8 + $proxPergunta) . '. Utilize o mouse e o retorno de áudio para identificar o gráfico e depois selecione a alternativa que corresponde ao gráfico.</p>';
                } else if ($imagem == 'BARRASROTULO') {
                    echo '<p tabindex="1" class="inicio">' . (8 + $proxPergunta) . '. Utilize o mouse e o retorno de áudio para responder a próxima pergunta.</p>';
                    echo '<div tabindex="2"> 
                            <p>O gráfico de colunas desta página representa a quantidade de acessos à um determinado Website. O total de acessos está agrupada de acordo com o provedor de internet utilizado pelo visitante (A, B, C ou D).</p>
                            <p>Utilize o mouse e o retorno de áudio para identificar o gráfico e depois selecione a alternativa que representa o provedor mais utilizado pelos visitantes deste Website.</p>
                          </div>';
                } else {
                    echo '<p tabindex="1" class="inicio">' . (8 + $proxPergunta) . '. Utilize o mouse e o retorno de áudio para responder a próxima pergunta.</p>';
                    echo '<div tabindex="2"> 
                            <p>O gráfico de colunas desta página representa a altural (em metros) dos prédios mais altos do mundo.</p>
                            <p>Utilize o mouse e o retorno de áudio para identificar o gráfico e depois selecione a alternativa que representa o prédio mais alto entre eles.</p>
                          </div>';
                }
                ?>
                <div class="radioOption">
                    <label for="opc1"><?php echo $opc1; ?></label>
                    <input type="radio" tabindex="3" id="opc1" name="figura" value="<?php echo $opc1; ?>"/>
                </div>
                <div class="radioOption">
                    <label for="opc2"><?php echo $opc2; ?></label>
                    <input type="radio" tabindex="3" id="opc2" name="figura" value="<?php echo $opc2; ?>"/>
                </div>
                <div class="radioOption">
                    <label for="opc3"><?php echo $opc3; ?></label>
                    <input type="radio" tabindex="3" id="opc3" name="figura" value="<?php echo $opc3; ?>"/> 
                </div>
                <div class="radioOption">
                    <label for="opc4"><?php echo $opc4; ?></label>
                    <input type="radio" tabindex="3" id="opc4" name="figura" value="<?php echo $opc4; ?>"/> 
                </div>
                <div class="radioOption">
                    <label for="opc5">Não Consegui Identificar</label>
                    <input type="radio" tabindex="3" id="opc5" name="figura" value="NÃO CONSEGUI IDENTIFICAR"/> 
                </div>                
            </div>            
        </form> 
        <div class="botoes">
            <input tabindex="4" type="button" value="Salvar Resposta" onclick="camposObrigatorios()"/>
            <input tabindex="5" type="button" value="Voltar" onclick="loadPage('controller/avaliacaoController.php?btnPressed=Voltar');"/>
        </div>
    </div>
</div>
<script>
    acessibilidade();

    function camposObrigatorios() {
        var resposta = getRadioValue('figura');
        if (resposta) {
            loadPage('controller/avaliacaoController.php');
        } else {
            alert('Por favor responda a pergunta');
        }
    }

    $(document).ready(function () {
<?php if (isset($respostaAnterior)) { ?>
            var respAnterior = '<?php echo $respostaAnterior; ?>';
            if ($('#opc1')[0].value.toUpperCase() == respAnterior.toUpperCase()) {
                $('#opc1').prop('checked', true);
                $('#opc2').prop('checked', false);
                $('#opc3').prop('checked', false);
                $('#opc4').prop('checked', false);
                $('#opc5').prop('checked', false);
            } else if ($('#opc2')[0].value.toUpperCase() == respAnterior.toUpperCase()) {
                $('#opc1').prop('checked', false);
                $('#opc2').prop('checked', true);
                $('#opc3').prop('checked', false);
                $('#opc4').prop('checked', false);
                $('#opc5').prop('checked', false);
            } else if ($('#opc3')[0].value.toUpperCase() == respAnterior.toUpperCase()) {
                $('#opc1').prop('checked', false);
                $('#opc2').prop('checked', false);
                $('#opc3').prop('checked', true);
                $('#opc4').prop('checked', false);
                $('#opc5').prop('checked', false);
            } else if ($('#opc4')[0].value.toUpperCase() == respAnterior.toUpperCase()) {
                $('#opc1').prop('checked', false);
                $('#opc2').prop('checked', false);
                $('#opc3').prop('checked', false);
                $('#opc4').prop('checked', true);
                $('#opc5').prop('checked', false);
            } else if ($('#opc5')[0].value.toUpperCase() == respAnterior.toUpperCase()) {
                $('#opc1').prop('checked', false);
                $('#opc2').prop('checked', false);
                $('#opc3').prop('checked', false);
                $('#opc4').prop('checked', false);
                $('#opc5').prop('checked', true);
            }

<?php } ?>
        $('.inicio')[0].focus();
    });
</script>